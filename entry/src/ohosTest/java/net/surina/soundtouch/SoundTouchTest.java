/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.surina.soundtouch;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import net.surina.soundtouch.util.ResUtil;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class SoundTouchTest {
    private SoundTouch mSoundTouch;
    private Context mContext;

    @Before
    public void setUp() {
        mContext = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        mSoundTouch = new SoundTouch();
    }

    private static final Object[] getTempoValues() {
        return new Object[]{
                new Object[]{1.0},
                new Object[]{3.0},
                new Object[]{5.0}
        };
    }

    private static final Object[] getPitchValues() {
        return new Object[]{
                new Object[]{-0.318},
                new Object[]{-0.525},
                new Object[]{0.788}
        };
    }

    private static final Object[] getSpeedValues() {
        return new Object[]{
                new Object[]{1.0},
                new Object[]{2.0},
                new Object[]{3.0}
        };
    }

    @Test
    @Parameters(method = "getTempoValues")
    public void shouldSetTempoAndProcess(float tempo) {
        mSoundTouch.setTempo(tempo);
        int res = mSoundTouch.processFile(ResUtil.getAudioInputFilePath(mContext), ResUtil.AUDIO_OUTPUT_PATH);
        //if res is 0 then processing is successful
        assertEquals(0, res);
    }

    @Test
    @Parameters(method = "getPitchValues")
    public void shouldSetPitchAndProcess(float pitch) {
        mSoundTouch.setPitchSemiTones(pitch);
        int res = mSoundTouch.processFile(ResUtil.getAudioInputFilePath(mContext), ResUtil.AUDIO_OUTPUT_PATH);
        //if res is 0 then processing is successful
        assertEquals(0, res);
    }

    @Test
    @Parameters(method = "getSpeedValues")
    public void shouldSetSpeedAndProcess(float speed) {
        mSoundTouch.setSpeed(speed);
        int res = mSoundTouch.processFile(ResUtil.getAudioInputFilePath(mContext), ResUtil.AUDIO_OUTPUT_PATH);
        //if res is 0 then processing is successful
        assertEquals(0, res);
    }

}
